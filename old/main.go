package main

import (
	"fmt"
	"net/http"
	"runtime"
	"strings"
	"time"

	"github.com/golang/glog"
)

func getVersion(w http.ResponseWriter, req *http.Request) {
	version := runtime.Version()
	w.Header().Add("version", version)
	fmt.Fprintf(w, "version: %s\n", version)
}

func healthz(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(w, "200\n")
}

func getHeaders(w http.ResponseWriter, req *http.Request) {
	for name, headers := range req.Header {
		for _, h := range headers {
			w.Header().Set(name, h)
		}
	}
}

func logs(w http.ResponseWriter, req *http.Request) {
	now := time.Now().Format("2006-01-02 15:04:05")
	remoteIP := strings.Split(req.RemoteAddr, ":")[0]
	// if remoteIP != "192.168.20.232" {
	// 	glog.Warningf("Remote")
	// }
	statusCode := http.StatusOK
	w.Header().Add("statusCode", "200")
	path := req.RequestURI
	glog.Infof("%s --- %s --- %s --- %d", now, remoteIP, path, statusCode)
}

func main() {
	http.HandleFunc("/version", getVersion)
	http.HandleFunc("/healthz", healthz)
	http.HandleFunc("/", getHeaders)
	http.HandleFunc("/logs", logs)
	http.ListenAndServe(":80", nil)
}
