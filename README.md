
# 云原生作业

## 监控
curl http://127.0.0.1:8080/metrics

## 最新镜像名称：
wangqian2182/wangqian2182:v3.0-metrics

## 时延url：
curl http://127.0.0.1:8080/hello

## prometheus采集

prometheus查询延时指标数据
httpserver_execution_latency_seconds_bucket httpserver_execution_latency_seconds_count httpserver_execution_latency_seconds_sum

grafana prometheus可视化显示
import httserve-latency.json